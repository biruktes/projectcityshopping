/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Database manager for city shopping database
 *
 * @author Bereketab Gulai
 * @date 19/11/2017
 * @References
 * https://stackoverflow.com/questions/23571095/declaring-foreign-key-in-apache-derby-database
 * https://db.apache.org/derby/docs/10.2/ref/rrefsqlj24513.html
 * https://www.techonthenet.com/sql_server/foreign_keys/foreign_delete.php
 * https://www.javatpoint.com/PreparedStatement-interface
 * https://stackoverflow.com/questions/15578912/how-to-join-three-tables-in-apache-derby-database
 */
public class DbHandler implements CrudInterface {

    // DB constants, it is important for maintanence
    // DB Setup constants
    private final String DB_NAME = "CityShoppingDb";
    private final String JDBC_URL = "jdbc:derby:" + DB_NAME + ";create=true";

    // Tables
    private final String TABLE_USERS = "users";
    private final String TABLE_CUSTOMERS = "customers";
    private final String TABLE_STAFF = "staff";
    private final String TABLE_PRODUCTS = "products";
    private final String TABLE_CLOTHING = "clothing";
    private final String TABLE_FOOTWEAR = "footwear";
    private final String TABLE_ORDERS = "orders";
    private final String TABLE_ORDER_LINES = "orderLines";

    // Users table
    private final String COLUMN_FIRSTNAME = "firstName";
    private final String COLUMN_LASTNAME = "lastName";
    private final String COLUMN_USERNAME = "username";
    private final String COLUMN_PASSWORD = "password";

    // Customers table
    private final String COLUMN_ADDRESS_LINE_1 = "addressLine1";
    private final String COLUMN_ADDRESS_LINE_2 = "addressLine2";
    private final String COLUMN_TOWN = "town";
    private final String COLUMN_POSTCODE = "postcode";

    // Staff table
    private final String COLUMN_POSITION = "position";
    private final String COLUMN_SALARY = "salary";

    // Products table
    private final String COLUMN_PRODUCT_ID = "productId";
    private final String COLUMN_PRODUCT_NAME = "productName";
    private final String COLUMN_PRICE = "price";
    private final String COLUMN_STOCKLEVEL = "stockLevel";

    // Clothing table
    private final String COLUMN_MEASUREMENT = "measurement";

    // Footwear table
    private final String COLUMN_SIZE = "size";

    // Orders table
    private final String COLUMN_ORDER_ID = "orderId";
    private final String COLUMN_ORDER_DATE = "orderDate";
    private final String COLUMN_STATUS = "status";

    // OrderLines table
    private final String COLUMN_ORDER_LINE_ID = "orderLineId";
    private final String COLUMN_QUANTITY = "quantity";

    public DbHandler() {
        try (Connection connection = getConnection()) {
            if (connection != null) {
                System.out.println("connected");
                // Makes tables have not been created
                if (!tablesExist()) {
                    createTable();
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Create db tables
     */
    private void createTable() {

        // ON DELETE CASCADE, will delete child also (primary in another table is deleted, so do references)
        String createTableUsers = "CREATE TABLE " + TABLE_USERS + " ("
                + COLUMN_FIRSTNAME + " VARCHAR(30) NOT NULL, "
                + COLUMN_LASTNAME + " VARCHAR(30) NOT NULL, "
                + COLUMN_USERNAME + " VARCHAR(20) NOT NULL PRIMARY KEY, "
                + COLUMN_PASSWORD + " VARCHAR(20) NOT NULL)";

        String createTableStaff = "CREATE TABLE " + TABLE_STAFF + " ("
                + COLUMN_USERNAME + " VARCHAR(20) NOT NULL REFERENCES " + TABLE_USERS + " (" + COLUMN_USERNAME + ") ON DELETE CASCADE, "
                + COLUMN_POSITION + " VARCHAR(20) NOT NULL, "
                + COLUMN_SALARY + " FLOAT NOT NULL, PRIMARY KEY (" + COLUMN_USERNAME + "))";

        String createTableCust = "CREATE TABLE " + TABLE_CUSTOMERS + " ("
                + COLUMN_USERNAME + " VARCHAR(20) NOT NULL REFERENCES " + TABLE_USERS + " (" + COLUMN_USERNAME + ") ON DELETE CASCADE, "
                + COLUMN_ADDRESS_LINE_1 + " VARCHAR(50) NOT NULL, "
                + COLUMN_ADDRESS_LINE_2 + " VARCHAR(50), "
                + COLUMN_TOWN + " VARCHAR(20) NOT NULL, "
                + COLUMN_POSTCODE + " CHAR(8) NOT NULL, PRIMARY KEY (" + COLUMN_USERNAME + "))";

        String createTableProducts = "CREATE TABLE " + TABLE_PRODUCTS + " ("
                + COLUMN_PRODUCT_ID + " INT NOT NULL PRIMARY KEY, "
                + COLUMN_PRODUCT_NAME + " VARCHAR(50) NOT NULL, "
                + COLUMN_PRICE + " FLOAT NOT NULL, "
                + COLUMN_STOCKLEVEL + " INT NOT NULL )";

        String createTableCloth = "CREATE TABLE " + TABLE_CLOTHING + " ("
                + COLUMN_PRODUCT_ID + " INT NOT NULL  REFERENCES " + TABLE_PRODUCTS + " (" + COLUMN_PRODUCT_ID + ") ON DELETE CASCADE, "
                + COLUMN_MEASUREMENT + " VARCHAR(50) NOT NULL)";

        String createTableFootwear = "CREATE TABLE " + TABLE_FOOTWEAR + " ("
                + COLUMN_PRODUCT_ID + " INT NOT NULL  REFERENCES " + TABLE_PRODUCTS + " (" + COLUMN_PRODUCT_ID + ") ON DELETE CASCADE, "
                + COLUMN_SIZE + " VARCHAR(50) NOT NULL)";

        // TODO review this relationship
        String createTableOrders = "CREATE TABLE " + TABLE_ORDERS + " ("
                + COLUMN_ORDER_ID + " INT NOT NULL, "
                + COLUMN_ORDER_DATE + " DATE NOT NULL, "
                + COLUMN_STATUS + " VARCHAR(20) NOT NULL, "
                + COLUMN_USERNAME + " VARCHAR(20) NOT NULL REFERENCES " + TABLE_USERS + " (" + COLUMN_USERNAME + ") ON DELETE CASCADE, "
                + "PRIMARY KEY (" + COLUMN_ORDER_ID + "))";

        // Somethings are better left an referenced, might be usefult to prevent referencial integrity issues, (product id)
        String createTableOrderLines = "CREATE TABLE " + TABLE_ORDER_LINES + " ("
                + COLUMN_ORDER_LINE_ID + " INT NOT NULL PRIMARY KEY, "
                + COLUMN_QUANTITY + " INT NOT NULL, "
                + COLUMN_PRODUCT_ID + " INT, "
                + COLUMN_ORDER_ID + " INT NOT NULL REFERENCES " + TABLE_ORDERS + " (" + COLUMN_ORDER_ID + ") ON DELETE CASCADE)";
        Statement statement;
        try {
            // Resource manager, will close connection when it is out of scope
            try (Connection connection = getConnection()) {
                statement = connection.createStatement();
                // Order of execution matters, as tables are being referenced
                statement.execute(createTableUsers);
                statement.execute(createTableStaff);
                statement.execute(createTableCust);
                statement.execute(createTableProducts);
                statement.execute(createTableCloth);
                statement.execute(createTableFootwear);
                statement.execute(createTableOrders);
                statement.execute(createTableOrderLines);
                System.out.print("created");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Connects to the db
     *
     * @return Connection to db
     */
    private Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(JDBC_URL);
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }

    /**
     * Checks if tables exist
     *
     * @return {@code true} indicates tables exist, {@code false} indicates
     * tables don't exist
     */
    private boolean tablesExist() {
        ResultSet resSet = null;
        boolean result = false;
        try {
            try (Connection connection = getConnection()) {
                resSet = connection.getMetaData().getTables(DB_NAME, "APP", "%", null);
                result = resSet.next();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    /**
     * Checks if the username specified exists
     *
     * @param usernameIn - the username to check
     * @return  <code>true</code> indicates username exists, otherwise
     * <code>false</code>
     */
    public boolean checkUsername(String usernameIn) {
        boolean userExists = false;
        try (Connection connection = getConnection()) {
            String getUserQry = "SELECT * FROM " + TABLE_USERS + " WHERE " + COLUMN_USERNAME + " = ?";

            // Check from users table
            PreparedStatement preStatement = connection.prepareStatement(getUserQry);
            preStatement.setString(1, usernameIn);

            ResultSet resultSet = preStatement.executeQuery();

            // Check user is found
            while (resultSet.next()) {
                userExists = true;
                System.out.println("User: " + usernameIn + " Exists.");
            }

            preStatement.close();
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return userExists;
    }

    /**
     * Checks if the product id specified exists
     *
     * @param productIdIn - the id to check
     * @return <code>true</code> indicates id exists, otherwise
     * <code>false</code>
     */
    public boolean checkProductId(int productIdIn) {
        boolean productExists = false;
        try (Connection connection = getConnection()) {
            String getProdcutQry = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + COLUMN_PRODUCT_ID + " = ?";

            // Check from users table
            PreparedStatement preStatement = connection.prepareStatement(getProdcutQry);
            preStatement.setInt(1, productIdIn);

            ResultSet resultSet = preStatement.executeQuery();

            // Check user is found
            while (resultSet.next()) {
                productExists = true;
                System.out.println("User: " + productIdIn + " Exists.");
            }

            preStatement.close();
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return productExists;
    }

    @Override
    public int addCustomer(Customer customerIn) {
        int result = -1;
        try (Connection connection = getConnection()) {
            String addUserQry = "INSERT INTO " + TABLE_USERS + " VALUES (?,?,?,?)";
            String addCustQry = "INSERT INTO " + TABLE_CUSTOMERS + " VALUES (?,?,?,?,?)";

            // Update users table
            PreparedStatement preStatement = connection.prepareStatement(addUserQry);
            // first param represents the index of the parameter (starts from 1)
            // Order of params also specifies the column in the table, createTable
            preStatement.setString(1, customerIn.getFirstName());
            preStatement.setString(2, customerIn.getLastName());
            preStatement.setString(3, customerIn.getUsername());
            preStatement.setString(4, customerIn.getPassword());

            result = preStatement.executeUpdate();

            // Update customers table
            preStatement = connection.prepareStatement(addCustQry);
            preStatement.setString(1, customerIn.getUsername());
            preStatement.setString(2, customerIn.getAddressLine1());
            preStatement.setString(3, customerIn.getAddressLine2());
            preStatement.setString(4, customerIn.getTown());
            preStatement.setString(5, customerIn.getPostcode());

            result = preStatement.executeUpdate();

            preStatement.close();

            // There is no need to verify this, as errors occur process will terminate before hann
            System.out.println("Row added to customers and users table...");
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @Override
    public int addStaff(Staff staffIn) {
        int result = -1;

        try (Connection connection = getConnection()) {
            String addUserQry = "INSERT INTO " + TABLE_USERS + " VALUES (?,?,?,?)";
            String addStaffQry = "INSERT INTO " + TABLE_STAFF + " VALUES (?,?,?)";

            // Update users table
            PreparedStatement preStatement = connection.prepareStatement(addUserQry);
            preStatement.setString(1, staffIn.getFirstName());
            preStatement.setString(2, staffIn.getLastName());
            preStatement.setString(3, staffIn.getUsername());
            preStatement.setString(4, staffIn.getPassword());

            result = preStatement.executeUpdate();

            // Update staff table
            preStatement = connection.prepareStatement(addStaffQry);
            preStatement.setString(1, staffIn.getUsername());
            preStatement.setString(2, staffIn.getPosition());
            preStatement.setDouble(3, staffIn.getSalary());

            result = preStatement.executeUpdate();

            preStatement.close();

            System.out.println("Row added to staff and users table...");
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @Override
    public int addCloth(Clothing clothingIn) {
        int result = -1;
        try (Connection connection = getConnection()) {
            String addProductQry = "INSERT INTO " + TABLE_PRODUCTS + " VALUES (?,?,?,?)";
            String addClothingQry = "INSERT INTO " + TABLE_CLOTHING + " VALUES (?,?)";

            // Add to products table
            PreparedStatement preStatement = connection.prepareStatement(addProductQry);
            preStatement.setInt(1, clothingIn.getProductId());
            preStatement.setString(2, clothingIn.getProductName());
            preStatement.setDouble(3, clothingIn.getPrice());
            preStatement.setInt(4, clothingIn.getStockLevel());

            result = preStatement.executeUpdate();

            // Add to clothing table
            preStatement = connection.prepareStatement(addClothingQry);
            preStatement.setInt(1, clothingIn.getProductId());
            preStatement.setString(2, clothingIn.getMeasurement());

            result = preStatement.executeUpdate();

            preStatement.close();

            System.out.println("Row inserted to tables products and clothing...");
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    @Override
    public int addFootwear(Footwear footwearIn) {
        int result = -1;
        try (Connection connection = getConnection()) {
            String addProductQry = "INSERT INTO " + TABLE_PRODUCTS + " VALUES (?,?,?,?)";
            String addFootwearQry = "INSERT INTO " + TABLE_FOOTWEAR + " VALUES (?,?)";

            // Add to products table
            PreparedStatement preStatement = connection.prepareStatement(addProductQry);
            preStatement.setInt(1, footwearIn.getProductId());
            preStatement.setString(2, footwearIn.getProductName());
            preStatement.setDouble(3, footwearIn.getPrice());
            preStatement.setInt(4, footwearIn.getStockLevel());

            result = preStatement.executeUpdate();

            // Add to footwear table
            preStatement = connection.prepareStatement(addFootwearQry);
            preStatement.setInt(1, footwearIn.getProductId());
            preStatement.setInt(2, footwearIn.getSize());

            result = preStatement.executeUpdate();

            preStatement.close();

            System.out.println("Row inserted to tables products and footwear...");

        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @Override
    public int addOrder(Order orderIn, String usernameIn) {
        int result = -1;
        try (Connection connection = getConnection()) {

            String addOrderQry = "INSERT INTO " + TABLE_ORDERS + " VALUES (?,?,?,?)";
            String addOrderLineQry = "INSERT INTO " + TABLE_ORDER_LINES + " VALUES (?,?,?,?)";

            // Insert to orders table
            PreparedStatement preStatement = connection.prepareStatement(addOrderQry);
            preStatement.setInt(1, orderIn.getOrderId());
            preStatement.setTimestamp(2, new java.sql.Timestamp(orderIn.getOrderDate().getTime()));
            preStatement.setString(3, orderIn.getStatus());
            preStatement.setString(4, usernameIn);

            result = preStatement.executeUpdate();

            // Insert to order line table
            preStatement = connection.prepareStatement(addOrderLineQry);

            // Loop through the orderlines in the hashmap
            for (OrderLine orderLine : orderIn.getOrderLines().values()) {
                preStatement.setInt(1, orderLine.getOrderLineId());
                preStatement.setInt(2, orderLine.getQuantity());
                preStatement.setInt(3, orderLine.getProduct().getProductId());
                preStatement.setInt(4, orderIn.getOrderId());

                // Execute for each orderLine
                result = preStatement.executeUpdate();
            }

            preStatement.close();

            System.out.println("Row added to tables orders and orderlines...");
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @Override
    public int addOrderLine(OrderLine orderLineIn, int orderIdIn) {
        int result = -1;
        try (Connection connection = getConnection()) {

            String addOrderLineQry = "INSERT INTO " + TABLE_ORDER_LINES + " VALUES (?,?,?,?)";

            // Insert to order line table
            try (PreparedStatement preStatement = connection.prepareStatement(addOrderLineQry)) {
                preStatement.setInt(1, orderLineIn.getOrderLineId());
                preStatement.setInt(2, orderLineIn.getQuantity());
                preStatement.setInt(3, orderLineIn.getProduct().getProductId());
                preStatement.setInt(4, orderIdIn);
                result = preStatement.executeUpdate();
                System.out.println("Row added to table orderlines...");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @Override
    public Customer getCustomer(String username) {

        Customer customer = new Customer();

        try (Connection connection = getConnection()) {
            String getUserQry = "SELECT * FROM " + TABLE_USERS + " WHERE " + COLUMN_USERNAME + " = ?";
            String getCustQry = "SELECT * FROM " + TABLE_CUSTOMERS + " WHERE " + COLUMN_USERNAME + " = ?";

            // Retrieve from users table
            PreparedStatement preStatement = connection.prepareStatement(getUserQry);
            preStatement.setString(1, username);

            ResultSet resultSet = preStatement.executeQuery();

            while (resultSet.next()) {
                customer.setFirstName(resultSet.getString(COLUMN_FIRSTNAME));
                customer.setLastName(resultSet.getString(COLUMN_LASTNAME));
                customer.setUsername(resultSet.getString(COLUMN_USERNAME));
                customer.setPassword(resultSet.getString(COLUMN_PASSWORD));
            }

            // Retrieve from customers table
            preStatement = connection.prepareStatement(getCustQry);
            preStatement.setString(1, username);
            resultSet = preStatement.executeQuery();

            while (resultSet.next()) {
                customer.setAddressLine1(resultSet.getString(COLUMN_ADDRESS_LINE_1));
                customer.setAddressLine2(resultSet.getString(COLUMN_ADDRESS_LINE_2));
                customer.setTown(resultSet.getString(COLUMN_TOWN));
                customer.setPostcode(resultSet.getString(COLUMN_POSTCODE));

                System.out.println("Customer: " + username + " Retrieved...");
            }

            resultSet.close();
            preStatement.close();
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        // If nothing, then empty object
        return customer;
    }

    @Override
    public Staff getStaff(String username) {

        Staff staff = new Staff();

        try (Connection connection = getConnection()) {
            String getUserQry = "SELECT * FROM " + TABLE_USERS + " WHERE " + COLUMN_USERNAME + " = ?";
            String getStaffQry = "SELECT * FROM " + TABLE_STAFF + " WHERE " + COLUMN_USERNAME + " = ?";

            // Retrieve from users table
            PreparedStatement preStatement = connection.prepareStatement(getUserQry);
            preStatement.setString(1, username);

            ResultSet resultSet = preStatement.executeQuery();

            while (resultSet.next()) {
                staff.setFirstName(resultSet.getString(COLUMN_FIRSTNAME));
                staff.setLastName(resultSet.getString(COLUMN_LASTNAME));
                staff.setUsername(resultSet.getString(COLUMN_USERNAME));
                staff.setPassword(resultSet.getString(COLUMN_PASSWORD));
            }

            // Retrieve from staff table
            preStatement = connection.prepareStatement(getStaffQry);
            preStatement.setString(1, username);
            resultSet = preStatement.executeQuery();

            while (resultSet.next()) {
                staff.setPosition(resultSet.getString(COLUMN_POSITION));
                staff.setSalary(resultSet.getDouble(COLUMN_SALARY));
            }

            resultSet.close();

        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        // If nothing, then empty object
        return staff;
    }

    @Override // TODO add limitations to getClothing and getFootwears
    public HashMap<Integer, Clothing> getClothings() {
        HashMap<Integer, Clothing> clothingProducts = new HashMap<>();

        try (Connection connection = getConnection()) {
            String getClothingQry = "SELECT * FROM " + TABLE_CLOTHING + " JOIN " + TABLE_PRODUCTS + " USING (" + COLUMN_PRODUCT_ID + ")";

            PreparedStatement preStatement;
            ResultSet resultSet;

            // Retrieve the all of the info from clothing table
            preStatement = connection.prepareStatement(getClothingQry);
            resultSet = preStatement.executeQuery();

            while (resultSet.next()) {
                Clothing clothing = new Clothing();
                clothing.setProductId(resultSet.getInt(COLUMN_PRODUCT_ID));
                clothing.setProductName(resultSet.getString(COLUMN_PRODUCT_NAME));
                clothing.setPrice(resultSet.getDouble(COLUMN_PRICE));
                clothing.setStockLevel(resultSet.getInt(COLUMN_STOCKLEVEL));
                clothing.setMeasurement(resultSet.getString(COLUMN_MEASUREMENT));

                clothingProducts.put(clothing.getProductId(), clothing);
            }

            System.out.println("Clothing products retrieved products and clothing table...");

        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return clothingProducts;
    }

    @Override
    public HashMap<Integer, Footwear> getFootwears() {
        HashMap<Integer, Footwear> footwearProducts = new HashMap<>();

        try (Connection connection = getConnection()) {
            String getFootwearQry = "SELECT * FROM " + TABLE_FOOTWEAR + " JOIN " + TABLE_PRODUCTS + " USING (" + COLUMN_PRODUCT_ID + ")";
            PreparedStatement preStatement;
            ResultSet resultSet;

            // Retrieve the rest of the info from clothing table
            preStatement = connection.prepareStatement(getFootwearQry);
            resultSet = preStatement.executeQuery();

            while (resultSet.next()) {
                Footwear footwear = new Footwear();
                footwear.setProductId(resultSet.getInt(COLUMN_PRODUCT_ID));
                footwear.setProductName(resultSet.getString(COLUMN_PRODUCT_NAME));
                footwear.setPrice(resultSet.getDouble(COLUMN_PRICE));
                footwear.setStockLevel(resultSet.getInt(COLUMN_STOCKLEVEL));
                footwear.setSize(resultSet.getInt(COLUMN_SIZE));

                footwearProducts.put(footwear.getProductId(), footwear);
            }

            System.out.println("Footwear products retrieved from tables products and footwear...");

        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return footwearProducts;
    }

    @Override
    public HashMap<Integer, Order> getOrders(String usernameIn, String statusIn) {

        HashMap<Integer, Order> orders = new HashMap<>();

        try (Connection connection = getConnection()) {

            String joinedOrderAndOrderLineQry = "SELECT * "
                    + "FROM " + TABLE_ORDERS + " JOIN " + TABLE_ORDER_LINES + " ON " + TABLE_ORDER_LINES + "." + COLUMN_ORDER_ID + " = "
                    + TABLE_ORDERS + "." + COLUMN_ORDER_ID + " JOIN " + TABLE_PRODUCTS + " ON "
                    + TABLE_PRODUCTS + "." + COLUMN_PRODUCT_ID + " = " + TABLE_ORDER_LINES + "." + COLUMN_PRODUCT_ID
                    + " WHERE " + TABLE_ORDERS + "." + COLUMN_USERNAME + " = ? AND " + TABLE_ORDERS + "." + COLUMN_STATUS + " = ?";

            PreparedStatement preStatement;
            ResultSet resultSet;
            int orderId;

            // Retrieve info from oders and orderLines table
            preStatement = connection.prepareStatement(joinedOrderAndOrderLineQry);
            preStatement.setString(1, usernameIn);
            preStatement.setString(2, statusIn);

            resultSet = preStatement.executeQuery();

            while (resultSet.next()) {
                boolean isRegistered;

                Order order = new Order();
                orderId = resultSet.getInt(COLUMN_ORDER_ID);
                order.setOrderId(orderId);
                order.setOrderDate(resultSet.getTimestamp(COLUMN_ORDER_DATE));
                order.setStatus(resultSet.getString(COLUMN_STATUS));

                OrderLine orderLine = new OrderLine();
                orderLine.setOrderLineId(resultSet.getInt(COLUMN_ORDER_LINE_ID));
                orderLine.setQuantity(resultSet.getInt(COLUMN_QUANTITY));

                // Product may be deleted, there is no control over it
                // Thus setting the id here is okay
                Product product = new Product();
                product.setProductId(resultSet.getInt(COLUMN_PRODUCT_ID));
                product.setProductName(resultSet.getString(COLUMN_PRODUCT_NAME));
                product.setPrice(resultSet.getDouble(COLUMN_PRICE));
                product.setStockLevel(resultSet.getInt(COLUMN_STOCKLEVEL));

                orderLine.setProduct(product);
                // Calculate total for the line 
                orderLine.setLineTotal(product.getPrice() * orderLine.getQuantity());

                isRegistered = order.getStatus().equals(Constants.PURCHASED);

                if (orders.containsKey(orderId)) {
                    orders.get(orderId).setOrderTotal(orderLine.getLineTotal());
                    orders.get(orderId).addOrderLine(orderLine, isRegistered);
                } else {
                    order.setOrderTotal(orderLine.getLineTotal());
                    order.addOrderLine(orderLine, isRegistered);
                    orders.put(orderId, order);
                }
                System.out.println("Order retrieved...");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return orders;
    }

    @Override // TODO needs to fixed, this code is old
    public HashMap<String, Customer> getOrders(String statusIn) {

        HashMap<String, Customer> customers = new HashMap<>();

        try (Connection connection = getConnection()) {

            String joinedOrderAndOrderLineQry = "SELECT * " + "FROM " + TABLE_ORDERS
                    + " JOIN " + TABLE_ORDER_LINES + " USING (" + COLUMN_ORDER_ID
                    + ") JOIN " + TABLE_PRODUCTS + " USING (" + COLUMN_PRODUCT_ID
                    + ") WHERE " + TABLE_ORDERS + "." + COLUMN_STATUS + " = ?";

            // Retrieve info from oders and orderLines table
            try (PreparedStatement preStatement = connection.prepareStatement(joinedOrderAndOrderLineQry)) {
                preStatement.setString(1, statusIn);

                ResultSet resultSet = preStatement.executeQuery();

                while (resultSet.next()) {
                    boolean isRegistered;
                    boolean isOrderSaved = false;
                    String username = resultSet.getString(COLUMN_USERNAME);
                    int orderId = resultSet.getInt(COLUMN_ORDER_ID);

                    Order order = new Order();
                    order.setOrderId(orderId);
                    order.setOrderDate(resultSet.getTimestamp(COLUMN_ORDER_DATE));
                    order.setStatus(resultSet.getString(COLUMN_STATUS));

                    OrderLine orderLine = new OrderLine();
                    orderLine.setOrderLineId(resultSet.getInt(COLUMN_ORDER_LINE_ID));
                    orderLine.setQuantity(resultSet.getInt(COLUMN_QUANTITY));

                    // Product may be deleted, there is no control over it
                    // Thus setting the id here is okay
                    Product product = new Product();
                    product.setProductId(resultSet.getInt(COLUMN_PRODUCT_ID));
                    product.setProductName(resultSet.getString(COLUMN_PRODUCT_NAME));
                    product.setPrice(resultSet.getDouble(COLUMN_PRICE));
                    product.setStockLevel(resultSet.getInt(COLUMN_STOCKLEVEL));

                    orderLine.setProduct(product);
                    // Calculate total for the line 
                    orderLine.setLineTotal(product.getPrice() * orderLine.getQuantity());

                    isRegistered = order.getStatus().equals(Constants.PURCHASED);

                    for (Customer cust : customers.values()) {
                        // Make sure duplicate orderId are saved to one key
                        if (cust.getOrders().containsKey(orderId)) {
                            cust.getOrders().get(orderId).addOrderLine(orderLine, isRegistered);
                            cust.getOrders().get(orderId).setOrderTotal(orderLine.getLineTotal());
                            isOrderSaved = true;
                        }
                    }

                    if (!isOrderSaved) {
                        // Add line total to the order
                        order.setOrderTotal(orderLine.getLineTotal());
                        order.addOrderLine(orderLine, isRegistered);

                        // Make sure all orders which belong to one user are saved with one key
                        if (customers.containsKey(username)) {
                            // Save order 
                            customers.get(username).addOrder(order);
                        } else {
                            Customer customer = new Customer();
                            customer.setUsername(username);
                            customer.addOrder(order);

                            // Save customer
                            customers.put(username, customer);
                        }
                    }

                    System.out.println("Order retrieved...");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return customers;
    }

    @Override
    public int updateCustomer(Customer customer, boolean updateCustInfo, boolean updateUserInfo) {
        int result = -1;
        try (Connection connection = getConnection()) {

            PreparedStatement preStatement;

            // Only proceed if update is required
            // Update customers table
            if (updateCustInfo) {
                String updateCustQry = "UPDATE " + TABLE_CUSTOMERS + " SET "
                        + COLUMN_ADDRESS_LINE_1 + " = ?, "
                        + COLUMN_ADDRESS_LINE_2 + " = ?, "
                        + COLUMN_TOWN + " = ?, "
                        + COLUMN_POSTCODE + " = ? WHERE " + COLUMN_USERNAME + " = ?";

                // Update customers table
                preStatement = connection.prepareStatement(updateCustQry);
                preStatement.setString(1, customer.getAddressLine1());
                preStatement.setString(2, customer.getAddressLine2());
                preStatement.setString(3, customer.getTown());
                preStatement.setString(4, customer.getPostcode());
                preStatement.setString(5, customer.getUsername());

                result = preStatement.executeUpdate();

                System.out.println("Row update on customers table...");
            }

            // Update users table
            if (updateUserInfo) {
                String addUserQry = "UPDATE " + TABLE_USERS + " SET "
                        + COLUMN_FIRSTNAME + " = ?, "
                        + COLUMN_LASTNAME + " = ?, "
                        + COLUMN_PASSWORD + " = ? WHERE " + COLUMN_USERNAME + " = ?";
                preStatement = connection.prepareStatement(addUserQry);
                preStatement.setString(1, customer.getFirstName());
                preStatement.setString(2, customer.getLastName());
                preStatement.setString(3, customer.getPassword());
                preStatement.setString(4, customer.getUsername());

                result = preStatement.executeUpdate();

                System.out.println("Row updated on users table...");

            }
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    // <editor-fold defaultstate="collapsed" desc="updateStaff">
    @Override
    public int updateStaff(Staff staff, boolean updateStaffInfo, boolean updateUserInfo
    ) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

// TODO Uncomment update staff method implementation to use 
//        int result = -1;
//        try (Connection connection = getConnection()) {
//
//            PreparedStatement preStatement;
//
//            // Only proceed if update is required
//            if (updateStaffInfo) {
//                String updateStaffQry = "UPDATE " + TABLE_STAFF + " SET "
//                        + COLUMN_POSITION + " = ?, "
//                        + COLUMN_SALARY + " = ? WHERE " + COLUMN_USERNAME + " = ?";
//
//                // Update staff table
//                preStatement = connection.prepareStatement(updateStaffQry);
//                preStatement.setString(1, staff.getPosition());
//                preStatement.setDouble(2, staff.getSalary());
//                preStatement.setString(3, staff.getUsername());
//
//                result = preStatement.executeUpdate();
//
//                System.out.println("Row updated on staff table...");
//            }
//
//            // Update users table
//            if (updateUserInfo) {
//                String updateUserQry = "UPDATE " + TABLE_USERS + " SET "
//                        + COLUMN_FIRSTNAME + " = ?, "
//                        + COLUMN_LASTNAME + " = ?, "
//                        + COLUMN_PASSWORD + " = ? WHERE " + COLUMN_USERNAME + " = ?";
//                preStatement = connection.prepareStatement(updateUserQry);
//                preStatement.setString(1, staff.getFirstName());
//                preStatement.setString(2, staff.getLastName());
//                preStatement.setString(3, staff.getPassword());
//                preStatement.setString(4, staff.getUsername());
//
//                result = preStatement.executeUpdate();
//
//                System.out.println("Row updated on users table...");
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return result;
    }
    // </editor-fold>      

    // TODO finish this
    public int updateProduct(Product product) {
        int result = -1;
        if (product != null) {
            try (Connection connection = getConnection()) {

                PreparedStatement preStatement;

                // Update clothing table
                if (product instanceof Clothing) {
                    Clothing clothing = (Clothing) product;

                    String updateClothingQry = "UPDATE " + TABLE_CLOTHING + " SET "
                            + COLUMN_MEASUREMENT + " = ? WHERE " + COLUMN_PRODUCT_ID + " = ?";

                    preStatement = connection.prepareStatement(updateClothingQry);
                    preStatement.setString(1, clothing.getMeasurement());
                    preStatement.setInt(2, clothing.getProductId());

                    result = preStatement.executeUpdate();

                    System.out.println("Row updated on clothing table...");
                }

                // Update footwear table
                if (product instanceof Footwear) {
                    Footwear footwear = (Footwear) product;

                    String updateFootwearQry = "UPDATE " + TABLE_FOOTWEAR + " SET "
                            + COLUMN_SIZE + " = ? WHERE " + COLUMN_PRODUCT_ID + " = ?";

                    preStatement = connection.prepareStatement(updateFootwearQry);
                    preStatement.setInt(1, footwear.getSize());
                    preStatement.setInt(2, footwear.getProductId());

                    result = preStatement.executeUpdate();

                    System.out.println("Row updated on footwear table...");
                }

                // Update products table, get the superclass as foot & cloth are
                // subclasses and will be ignored if the product change is also true
                if (product.getClass().getSuperclass().isInstance(new Product())) {
                    String updateProductQry = "UPDATE " + TABLE_PRODUCTS + " SET "
                            + COLUMN_PRODUCT_NAME + " = ?, "
                            + COLUMN_PRICE + " = ?, "
                            + COLUMN_STOCKLEVEL + " = ? WHERE " + COLUMN_PRODUCT_ID + " = ?";
                    preStatement = connection.prepareStatement(updateProductQry);
                    preStatement.setString(1, product.getProductName());
                    preStatement.setDouble(2, product.getPrice());
                    preStatement.setInt(3, product.getStockLevel());
                    preStatement.setInt(4, product.getProductId());

                    result = preStatement.executeUpdate();

                    System.out.println("Row updated on products table...");
                }
            } catch (SQLException ex) {
                Logger.getLogger(DbHandler.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;

    }

    public int updateOrder(HashMap<Integer, Order> ordersIn, String usernameIn) {
        int result = -1;
        try (Connection connection = getConnection()) {

            String updateOrderQry = "UPDATE " + TABLE_ORDERS + " SET "
                    + COLUMN_ORDER_DATE + " = ?, " + COLUMN_STATUS
                    + " = ?, " + COLUMN_USERNAME + " = ?  WHERE " + COLUMN_ORDER_ID + " = ?";

            try (PreparedStatement preStatement = connection.prepareStatement(updateOrderQry)) {

                if (usernameIn != null && !usernameIn.equals("")) {
                    for (Order order : ordersIn.values()) {

                        preStatement.setDate(1, new java.sql.Date(order.getOrderDate().getTime()));
                        preStatement.setString(2, order.getStatus());
                        preStatement.setString(3, usernameIn);
                        preStatement.setInt(4, order.getOrderId());

                        result = preStatement.executeUpdate();

                        System.out.println("Column date, status and username updated on table orders...");
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @Override
    public int updateOrderLine(int orderLineIdIn, int quantityIn) {
        int result = -1;
        try (Connection connection = getConnection()) {

            String updateOrderQry = "UPDATE " + TABLE_ORDER_LINES + " SET "
                    + COLUMN_QUANTITY + " = ? WHERE " + COLUMN_ORDER_LINE_ID + " = ?";
            // Update to  table
            try (PreparedStatement preStatement = connection.prepareStatement(updateOrderQry)) {
                preStatement.setInt(1, quantityIn);
                preStatement.setInt(2, orderLineIdIn);

                result = preStatement.executeUpdate();
                System.out.println("Column quantity updated on table oderlines...");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @Override
    public int updateStockLevel(int productIdIn, int stockLevelIn) {
        int result = -1;

        try (Connection connection = getConnection()) {
            String updateProductQry = "UPDATE " + TABLE_PRODUCTS + " SET "
                    + COLUMN_STOCKLEVEL + " = ? WHERE " + COLUMN_PRODUCT_ID + " = ?";
            PreparedStatement preStatement = connection.prepareStatement(updateProductQry);
            preStatement.setInt(1, stockLevelIn);
            preStatement.setInt(2, productIdIn);

            result = preStatement.executeUpdate();

            if (result != -1) {
                System.out.println("Stocklevel updated on products table...");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @Override
    public int deleteUser(String usernameIn) {
        int result = -1;
        try (Connection connection = getConnection()) {
            String deleteUserQry = "DELETE FROM " + TABLE_USERS + " WHERE " + COLUMN_USERNAME + " = ?";

            try (PreparedStatement preStatement = connection.prepareStatement(deleteUserQry)) {
                preStatement.setString(1, usernameIn);
                result = preStatement.executeUpdate();

                System.out.println("User Deleted");
            }

        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @Override
    public int deleteProduct(int productIdIn) {
        int result = -1;
        try (Connection connection = getConnection()) {
            String deleteProductQry = "DELETE FROM " + TABLE_PRODUCTS + " WHERE " + COLUMN_PRODUCT_ID + " = ?";

            try (PreparedStatement preStatement = connection.prepareStatement(deleteProductQry)) {
                preStatement.setInt(1, productIdIn);

                result = preStatement.executeUpdate();

                System.out.println("Product Deleted");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @Override
    public int deleteOrderLine(int productIdIn) {
        int result = -1;
        try (Connection connection = getConnection()) {
            String deleteProductQry = "DELETE FROM " + TABLE_ORDER_LINES + " WHERE " + COLUMN_PRODUCT_ID + " = ?";

            try (PreparedStatement preStatement = connection.prepareStatement(deleteProductQry)) {
                preStatement.setInt(1, productIdIn);

                result = preStatement.executeUpdate();

                System.out.println("Orderline Deleted");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
