/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 * Creates an Order Line for Orders class
 *
 * @author Bereketab Gulai
 */
public class OrderLine {

    // Attributes 
    private int orderLineId;
    private int quantity;
    private Product product;

    // Computable information, not in db
    private double lineTotal;

    /**
     * Default Constructor
     */
    public OrderLine() {
    }

    /**
     * Overload Constructor
     *
     * @param orderIn
     * @param productIn
     * @param quantityIn
     */
    public OrderLine(Order orderIn, Product productIn, int quantityIn) {
        this.orderLineId = orderIn.generateUniqueOrderLineId();
        this.product = productIn;
        this.quantity = quantityIn;
        this.lineTotal = productIn.getPrice() * quantityIn;
    }

    // Getters and Setters
    public int getOrderLineId() {
        return orderLineId;
    }

    public void setOrderLineId(int orderLineIdIn) {
        this.orderLineId = orderLineIdIn;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product productIn) {
        this.product = productIn;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantityIn) {
        this.quantity = quantityIn;
    }

    public double getLineTotal() {
        return lineTotal;
    }

    public void setLineTotal(double lineTotalIn) {
        this.lineTotal = lineTotalIn;
    }
}
