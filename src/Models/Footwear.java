/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Biruk
 */
public class Footwear extends Product {

    // Attribute
    private int size;

    /**
     * Default constructor
     */
    public Footwear () {
        super();
    }
    
    /**
     * Overload Constructors, with all parameters
     * @param sizeIn
     * @param productIdIn
     * @param productNameIn
     * @param priceIn
     * @param stockLevelIn
     */
    public Footwear(int sizeIn, int productIdIn, String productNameIn, double priceIn, int stockLevelIn) {
        super(productIdIn, productNameIn, priceIn, stockLevelIn);
        this.size = sizeIn;
    }

    /**
     * Overload Constructors, without product id
     * @param size
     * @param productNameIn
     * @param priceIn
     * @param stockLevelIn
     */
    public Footwear(int size, String productNameIn, double priceIn, int stockLevelIn) {
        super(productNameIn, priceIn, stockLevelIn);
        this.size = size;
    }

    // Getter and Setter
    public int getSize() {
        return size;
    }

    public void setSize(int sizeIn) {
        this.size = sizeIn;
    }
}
