/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.HashMap;

/**
 * Creates a customer, methods include displayGreetings and more
 *
 * @author Bereketab Gulai
 */
public class Customer extends User {

    // Attributes
    private String addressLine1;
    private String addressLine2;
    private String town;
    private String postcode;

    // Computable information, not in db
    private HashMap<Integer, Order> orders;
    private boolean isRegistered;

    // Default Constructor
    public Customer() {
        this.orders = new HashMap<>();
        addressLine1 = "";
        addressLine2 = "";
        town = "";
        postcode = "";
    }

    // Overload Constructor
    /**
     *
     * @param firstNameIn
     * @param lastNameIn
     * @param usernameIn
     * @param passwordIn
     * @param addressLine1In
     * @param addressLine2In
     * @param townIn
     * @param postcodeIn
     */
    public Customer(String firstNameIn, String lastNameIn, String usernameIn, String passwordIn, String addressLine1In, String addressLine2In, String townIn, String postcodeIn) {
        super(firstNameIn, lastNameIn, usernameIn, passwordIn);
        this.addressLine1 = addressLine1In;
        this.addressLine2 = addressLine2In;
        this.town = townIn;
        this.postcode = postcodeIn;
        orders = new HashMap<>();
        isRegistered = true;
    }

    // Methods 
    public String displayGreetings() {
        return "Welcome " + this.getFirstName() + " " + this.getLastName();
    }

    @Override
    public String toString() {
        return "Name: " + this.getFirstName() + " " + this.getLastName() + "\n "
                + "Username: " + this.getUsername() + "\n "
                + "Address: " + this.getAddressLine1() + ", " + this.getAddressLine2() + ", "
                + this.getTown() + ", " + this.getPostcode() + "\n "
                + "Number of Orders: " + this.orders.size();
    }

    public void addOrder(Order orderIn) {
        orders.put(orderIn.getOrderId(), orderIn);
    }

    // Getters and Setters
    public HashMap<Integer, Order> findAllComplete() {
        HashMap<Integer, Order> orders = new HashMap<>();
        return orders;
    }

    public Order findLastOrder() {
        Order order = new Order();
        return order;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public HashMap<Integer, Order> getOrders() {
        // Shallow copy
        return new HashMap<>(orders);
    }

    public void setOrders(HashMap<Integer, Order> ordersIn) {
        // Shallow copy
        this.orders = new HashMap<>(ordersIn);
    }

    public boolean isIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(boolean isRegisteredIn) {
        this.isRegistered = isRegisteredIn;
    }

}
