/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 * @since 
 * @author Bereketab Gulai
 */
public class Staff extends User {

    // Attributes
    private String position;
    private double salary;
    
    /**
     * Default Constructor
     */
    public Staff() {
        super();
    }

    /**
     * Overload constructor, all parameters
     * @param positionIn
     * @param salaryIn
     * @param firstNameIn
     * @param lastNameIn
     * @param usernameIn
     * @param passwordIn 
     */
    public Staff(String positionIn, double salaryIn, String firstNameIn, String lastNameIn, String usernameIn, String passwordIn) {
        super(firstNameIn, lastNameIn, usernameIn, passwordIn);
        this.position = positionIn;
        this.salary = salaryIn;
    }


    // Methods
    public String displayGreetings() {
        return "Welcome " + this.getFirstName() + " " + this.getLastName();
    }

    // Getters and Setters
    public String getPosition() {
        return position;
    }

    public void setPosition(String positionIn) {
        this.position = positionIn;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salaryIn) {
        this.salary = salaryIn;
    }
}
