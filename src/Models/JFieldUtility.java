/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.awt.Component;
import java.util.ArrayList;
import javax.swing.JTextField;

/**
 * Provides method to clear, find empty, find first empty JTextField(s)
 *
 * @author Bereketab Gulai
 */
public final class JFieldUtility {

    /**
     * Clears all the text fields in the form. This works, as no new instance of
     * the JTextField is created rather passed around and modified. Thus they
     * reference to the same object in memory.
     *
     * @param componentsIn the array with all the components
     */
    public static void clearFields(Component[] componentsIn) {
        for (Component component : getJFields(componentsIn)) {
            JTextField jTextField = (JTextField) component;
            jTextField.setText("");
        }
    }

    /**
     * Checks if any text field is empty, a generic method
     *
     * @param componentsIn the array with all the components
     * @return <code>true</code> indicate field is empty, otherwise
     * <code>false</code>
     */
    public static boolean isFieldEmpty(Component[] componentsIn) {
        for (Component component : getJFields(componentsIn)) {
            JTextField txt = (JTextField) component;
            if (txt.getText().equals("")) {
                return true;
            }
        }

        return false;
    }

        /**
     * Checks if any text field is not empty, a generic method
     *
     * @param componentsIn the array with all the components
     * @return <code>true</code> indicate field is empty, otherwise
     * <code>false</code>
     */
    public static boolean isNotFieldEmpty(Component[] componentsIn) {
        for (Component component : getJFields(componentsIn)) {
            JTextField txt = (JTextField) component;
            if (!txt.getText().equals("")) {
                return true;
            }
        }

        return false;
    }
    
    /**
     * TODO dysfunctional don't use, Finds the first empty text field and returns
     *
     * @param componentIn the array with all the components
     * @return instance of the empty field
     */
//    public static JTextField findFirstEmptyField(Component[] componentIn) {
//        boolean isFieldEmpty = false;
//        // TODO sorting is required
//        for (Component component : getJFields(componentIn)) {
//            JTextField field = (JTextField) component;
//            if (!field.getText().equals("")) {
//                isFieldEmpty = true;
//            } else if (field.getText().equals("") && isFieldEmpty) {
//                return field;
//            }
//        }
//
//        return new JTextField();
//    }

    /**
     * Gets all the JTextField components
     *
     * @param componentIn the array with all the components
     * @return an array of JTextField components
     */
    public static ArrayList<Component> getJFields(Component[] componentIn) {
        ArrayList<Component> componentList = new ArrayList<>();
        for (Component component : componentIn) {
            if (component instanceof JTextField) {
                componentList.add(component);
            }
        }

        return componentList;
    }
}
