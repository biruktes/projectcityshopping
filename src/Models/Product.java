/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Biruk
 */
public class Product {

    // Attributes
    private int productId;
    private String productName;
    private double price;
    private int stockLevel;

    /**
     * Default constructor
     */
    public Product() {
    }

    /**
     * Overload Constructor, with all parameters
     *
     * @param productIdIn
     * @param productNameIn
     * @param priceIn
     * @param stockLevelIn
     */
    public Product(int productIdIn, String productNameIn, double priceIn, int stockLevelIn) {
        this.productId = productIdIn;
        this.productName = productNameIn;
        this.price = priceIn;
        this.stockLevel = stockLevelIn;
    }

    /**
     * Overload Constructor, without prodcutId
     *
     * @param productNameIn
     * @param priceIn
     * @param stockLevelIn
     */
    public Product(String productNameIn, double priceIn, int stockLevelIn) {
        this.productName = productNameIn;
        this.price = priceIn;
        this.stockLevel = stockLevelIn;
    }

    /**
     * Overload copy constructor
     *
     * @param productIn
     */
    public Product(Product productIn) {
        this.productId = productIn.getProductId();
        this.productName = productIn.getProductName();
        this.price = productIn.getPrice();
        this.stockLevel = productIn.getStockLevel();
    }

    // Methods
    @Override
    public String toString() {
        return "Product Id: " + productId + "\n "
                + "Product Name: " + productName + "\n "
                + "Price: " + "\n "
                + "StockLevel: " + "\n ";
    }

    // Getters and Setters
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productIdIn) {
        this.productId = productIdIn;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productNameIn) {
        this.productName = productNameIn;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double priceIn) {
        this.price = priceIn;
    }

    public int getStockLevel() {
        return stockLevel;
    }

    public void setStockLevel(int stockLevelIn) {
        this.stockLevel = stockLevelIn;
    }
}
