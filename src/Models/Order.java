/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Date;
import java.util.HashMap;
import java.util.Random;

/**
 * Creates an Order object
 *
 * @author Bereketab Gulai
 * @date 17/11/2017
 */
public class Order {

    // Attributes
    private int orderId;
    private Date orderDate;
    private String status;

    // Computable information, not in db
    private double orderTotal;
    private HashMap<Integer, OrderLine> orderLines;

    /**
     * Default Constructor
     */
    public Order() {
        orderId = 0;
        orderDate = new Date();
        this.orderLines = new HashMap<>();
    }

    /**
     * Overload constructor, all parameters
     *
     * @param orderIdIn
     * @param orderDateIn
     * @param orderTotalIn
     * @param statusIn
     */
    public Order(int orderIdIn, Date orderDateIn, double orderTotalIn, String statusIn) {
        this.orderId = orderIdIn;
        this.orderDate = orderDateIn;
        this.orderTotal = orderTotalIn;
        this.status = statusIn;
        this.orderLines = new HashMap<>();
    }

    /**
     * Adds an orderLine to the collection
     *
     * @param orderLineIn specifies the orderLine object
     * @param isRegisteredIn TODO find use for this parameter: isRegisteredIn
     */
    public void addOrderLine(OrderLine orderLineIn, boolean isRegisteredIn) {
        orderLines.put(orderLineIn.getOrderLineId(), orderLineIn);
    }

    /**
     * Remove an orderLine with the specified productId from the collection
     *
     * @param productIdIn specifies the product id
     * @param isRegisteredIn
     */
    public void removeOrderLine(int productIdIn, boolean isRegisteredIn) {
        for (OrderLine orderLine : orderLines.values()) {
            if (orderLine.getProduct().getProductId() == productIdIn) {
                orderLines.remove(orderLine.getOrderLineId());
            }
        }
    }

    /**
     * Gets the quantity of products for the specified product id
     *
     * @param productIdIn specifies the product id
     * @return  <code>-1</code>indicates productId does not exist otherwise
     * quantity
     */
    public int getQuantityOfProduct(int productIdIn) {
        for (OrderLine orderLine : orderLines.values()) {
            if (orderLine.getProduct().getProductId() == productIdIn) {
                return orderLine.getQuantity();
            }
        }
        return -1;
    }

    public int generateUniqueOrderLineId() {
        // Random from the given date value
        return Math.abs(((new Random()).nextInt(Math.abs((int) orderDate.getTime()))) / 10000);
    }

    // Getters and Setters
    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderIdIn) {
        this.orderId = orderIdIn;
    }

    public Date getOrderDate() {
        return new Date(orderDate.getTime());
    }

    public void setOrderDate(Date orderDateIn) {
        this.orderDate = new Date(orderDateIn.getTime());
    }

    public double getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(double orderTotalIn) {
        // Will added to the existing value if any
        this.orderTotal += orderTotalIn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String statusIn) {
        this.status = statusIn;
    }

    public HashMap<Integer, OrderLine> getOrderLines() {
        return new HashMap<>(orderLines);
    }

    public void setOrderLines(HashMap<Integer, OrderLine> orderLinesIn) {
        this.orderLines = new HashMap<>(orderLinesIn);
    }
}
