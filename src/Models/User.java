package Models;

/**
 * @since 09/11/2017
 * @author Bereketab Gulai
 */
public class User {

    // Attributes
    private String firstName;
    private String lastName;
    private String username;
    private String password;

    // Default constructor
    public User() {
        firstName = "";
        lastName = "";
        username = "";
        password = "";
    }

    // Overload constructor
    public User(String firstNameIn, String lastNameIn, String usernameIn, String passwordIn) {
        this.firstName = firstNameIn;
        this.lastName = lastNameIn;
        this.username = usernameIn;
        this.password = passwordIn;
    }

    // Getters and Setters
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstNameIn) {
        this.firstName = firstNameIn;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastNameIn) {
        this.lastName = lastNameIn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String usernameIn) {
        this.username = usernameIn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
