/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.HashMap;

/**
 * Provides methods to be overridden by class which implement this interface
 *
 * @author Bereketab Gulai
 * @date 19/11/2017
 *
 * Refs: https://stackoverflow.com/questions/7202616/java-abstract-interface
 * 'Interfaces and their methods are implicitly abstract and adding that
 * modifier makes no difference.'
 */
public interface CrudInterface {

    //-----Create-----
    /**
     * Adds a customer to the to users, and customers tables (referenced table)
     *
     * @param customerIn the user customer object to add
     * @return -1 for error or row count for success
     */
    int addCustomer(Customer customerIn);

    /**
     * Adds a staff to the users and staff tables
     *
     * @param staffIn the user staff object to add
     * @return -1 for error or row count for success
     */
    int addStaff(Staff staffIn);

    /**
     * Adds a cloth product to the products and clothing tables
     *
     * @param clothingIn the product clothing object to add
     * @return -1 for error or row count for success
     */
    int addCloth(Clothing clothingIn);

    /**
     * Adds a footwear product to the products and footwear tables
     *
     * @param footwearIn the product footwear object to add
     * @return -1 for error or row count for success
     */
    int addFootwear(Footwear footwearIn);

    /**
     * Adds an order/line to the orders and orderLines tables
     *
     * @param orderIn specifies the order object to store
     * @param usernameIn the username that the order belongs to
     * @return -1 for error or else row count for success
     */
    int addOrder(Order orderIn, String usernameIn);

    /**
     * Adds an orderLine to the orderLines table
     *
     * @param orderLineIn the orderLine object to add
     * @param orderIdIn sets the order id of the orderLine
     * @return
     */
    int addOrderLine(OrderLine orderLineIn, int orderIdIn);

    //-----Retrieve-----
    /**
     * Returns the customer with the associated username from customers and
     * users table
     *
     * @param usernameIn specifies which customer details
     * @return Customer object
     */
    Customer getCustomer(String usernameIn);

    /**
     * Returns the staff with the associated username from staff and users table
     *
     * @param usernameIn specifies which staff details
     * @return Staff object
     */
    Staff getStaff(String usernameIn);

    /**
     * Returns the clothing products with the associated product id from
     * clothing and products table, this would be inefficient on large scale,
     * however for now is it better than one by one
     *
     * @return HashMap object for all selected clothing products
     */
    HashMap<Integer, Clothing> getClothings();

    /**
     * Returns the footwear with the associated product id from footwear and
     * products table, this would be inefficient on large scale, however for now
     * is it better than one by one
     *
     * @return HashMap object for all selected footwear products
     */
    HashMap<Integer, Footwear> getFootwears();

    /**
     * Returns all the orders with the associated username and specified order
     * status from orders and the rest from orderLines and products table
     *
     * @param usernameIn the username the order belongs to
     * @param statusIn sets whether order is purchased or in-basket
     * @return HashMap object for all selected orders
     */
    HashMap<Integer, Order> getOrders(String usernameIn, String statusIn);

    /**
     * Returns all the orders with the specified order status from orders and
     * the rest from orderLines and products table
     *
     * @param statusIn sets whether order is purchased or in-basket
     * @return HashMap object for all selected orders
     */
    HashMap<String, Customer> getOrders(String statusIn);

    //-----Update-----
    /**
     * Updates a customer in the customers or/both users table
     *
     * @param customerIn the user customer to update
     * @param updateCustInfoIn indicate if customers table also needs to update
     * @param updateUserInfoIn indicate if users table also needs to update
     * @return <code>-1</code> for error or row count for success
     */
    int updateCustomer(Customer customerIn, boolean updateCustInfoIn, boolean updateUserInfoIn);

    /**
     * Updates a staff in the staff or/both users table
     *
     * @param staffIn the user staff to update
     * @param updateStaffInfoIn indicate if staff table also needs to update
     * @param updateUserInfoIn indicate if users table also needs to update
     * @return <code>-1</code> for error or row count for success
     */
    int updateStaff(Staff staffIn, boolean updateStaffInfoIn, boolean updateUserInfoIn);

    /**
     * Updates a clothing or footwear or product in the clothing or/both products table
     *
     * @param productIn the product to update
     * @return <code>-1</code> for error or row count for success
     */
    int updateProduct(Product productIn);

    /**
     * Updates an order username but also status if specified, with the orderId
     * in the orders table
     *
     * @param ordersIn the orders which belong to the username
     * @param usernameIn username of the new username
     * @return <code>-1</code> for error or else row count for success
     */
    int updateOrder(HashMap<Integer, Order> ordersIn, String usernameIn);

    /**
     * Updates an orderLine with increase in quantity in the orderLines tables
     *
     * @param orderLineIdIn specifies the orderLine id to update
     * @param quantityIn determines the value to update
     * @return <code>-1</code> for error or else row count for success
     */
    int updateOrderLine(int orderLineIdIn, int quantityIn);

        /**
     * Updates a stockLevel column identified by the product id in the products tables
     *
     * @param productIdIn specifies the product id to update
     * @param stockLevelIn determines the new stock level
     * @return <code>-1</code> for error or else row count for success
     */
    int updateStockLevel(int productIdIn, int stockLevelIn);

    //-----Delete-----
    /**
     * Deletes a user from the users table, also effects referencing tables
     *
     * @param usernameIn the username to delete
     * @return <code>-1</code> for error or row count for success
     */
    int deleteUser(String usernameIn);

    /**
     * Deletes a product from the product table, also effects referencing tables
     *
     * @param productIdIn the product id to delete
     * @return <code>-1</code> for error or row count for success
     */
    int deleteProduct(int productIdIn);

    /**
     * Deletes an orderLine from the orderLines table
     *
     * @param productIdIn sets the product id
     * @return <code>-1</code> for error or else row count for success
     */
    int deleteOrderLine(int productIdIn);
}
