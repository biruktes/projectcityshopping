/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 * Keeps all the global constants
 *
 * @author Bereketab Gulai
 */
public final class Constants {

    // Public Global variablesF
    /* Make sure these constant values are not changed either by unknowningly 
     * or other could cause many things to fail
     */
    /**
     * Indicates that the order has been checked out
     */
    public static final String PURCHASED = "Purchased";

    /**
     * Indicates that the order is still in basket
     */
    public static final String IN_BASKET = "In-Basket";

    private static final String CUSOTMER_FIRSTNAME = "Temp";
    private static final String CUSOTMER_LASTNAME = "Cust";
    private static final String CUSOTMER_USERNAME = "TempCust";

    /**
     * Creates a customer object to be use as a temporary customer while
     * browsing
     */
    public static final Customer CUSOTMER = new Customer(CUSOTMER_FIRSTNAME, CUSOTMER_LASTNAME, CUSOTMER_USERNAME, "", "", "", "", "");

    private static final String STAFF_FIRSTNAME = "Bereketab";
    private static final String STAFF_LASTNAME = "Gulai";
    private static final String STAFF_USERNAME = "admin";
    private static final String STAFF_PASSWORD = "aa";

    private static final String STAFF_POSITION = "Admin";
    private static final double STAFF_SALARY = 20000;

    /**
     * Creates a customer object to be use as a temporary customer while
     * browsing
     */
    public static final Staff STAFF_1 = new Staff(STAFF_POSITION, STAFF_SALARY, STAFF_FIRSTNAME, STAFF_LASTNAME, STAFF_USERNAME, STAFF_PASSWORD);

    // Messages
    public static final String MSG_PRODUCT_EXISTS = "Product already exists in your basket.\n \nWould you like to continue with selected quantity?";
    public static final String MSG_SELECT_QUANTITY = "Sorry, please select quantity.";
    public static final String MSG_UNREGISTER = "Are you sure you want to unregister?";
    public static final String MSG_REGISTER = "Sorry, you must register before you can purchase your items.\n \nWould you like to register?";
    public static final String MSG_USERNAME_NOT_FOUND = "Sorry, the username was not found.";
    public static final String MSG_ENTER_CORRECT_PASSWORD = "Sorry, please enter a correct password.";
    public static final String MSG_SELECT_TYPE = "Please select the type of product you would like to add.";
    public static final String MSG_FILL_FIELDS = "Sorry, please fill all the fields.";
    public static final String MSG_DELETE_PRODUCT = "Are you sure you want to delete this product?";
    public static final String MSG_PRODUCT_SUBMITTED = "Product submitted.";
    public static final String MSG_SELECT_PRODUCT = "Please select a product.";
    public static final String MSG_NO_CHANGES = "No changes found.";
    public static final String MSG_NO_PRODUCTS = "Sorry, we don't have products of this category currently.";
    public static final String MSG_FILL_CORRECTLY = "Sorry, please make sure fields are filled correctly.";
    public static final String MSG_ADDED_TO_BASKET = "The product was added to your basket.";
    public static final String MSG_SELECT_ORDER = "Please select an order";
    public static final String MSG_EMPTY_BASKET = "Sorry, your basket is empty.\n \nWould you like to add products?";
    public static final String MSG_OUT_OF_STOCK = "Sorry, we are out of stock of this product.";
}
