/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Biruk
 */
public class Clothing extends Product {

    // Attribute
    private String measurement;

    /**
     * Default constructor
     */
    public Clothing() {
        super();
    }

    /**
     * Overload constructors, all parameters
     *
     * @param measurementIn
     * @param productIdIn
     * @param productNameIn
     * @param priceIn
     * @param stockLevelIn
     */
    public Clothing(String measurementIn, int productIdIn, String productNameIn, double priceIn, int stockLevelIn) {
        super(productIdIn, productNameIn, priceIn, stockLevelIn);
        this.measurement = measurementIn;
    }

    /**
     * Overload constructors, without no product id
     * @param measurementIn
     * @param productNameIn
     * @param priceIn
     * @param stockLevelIn
     */
    public Clothing(String measurementIn, String productNameIn, double priceIn, int stockLevelIn) {
        super(productNameIn, priceIn, stockLevelIn);
        this.measurement = measurementIn;
    }

    // Getters and Setters
    public String getMeasurement() {
        return measurement;
    }

    public void setMeasurement(String measurementIn) {
        this.measurement = measurementIn;
    }
}
